# giteacat
This Python script allows sign up to a Gitea instance to all local users of a Mastodon server.

### Dependencies

-   **Python 3**
-   Gitea running server with admin access account
-   Mastodon's bot account  

## Usage  

From the Mastodon's server account where this bot is running:  

@bot_username registre  

## Installation  

1. Clone this repo: git clone https://git.mastodont.cat/spla/giteacat.git <target dir>  

2. cd into your <target dir> and create the Python Virtual Environment: `python3.x -m venv .`  

3. Activate the Python Virtual Environment: `source bin/activate`  

4. Run `pip install -r requirements.txt` to install needed libraries.  

5. Run `python setup.py` to setup the Mastodon bot account, bot's access tokens, gitea hostname and gitea access token (admin account).  

6. Use your favourite scheduling method to set `python giteacat.py` to run every minute. 


