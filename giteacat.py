from mastodon import Mastodon
import re
import os
import sys
import os.path
import string
import secrets
import requests
import pdb

def cleanhtml(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext

def unescape(s):
    s = s.replace("&apos;", "'")
    return s

def generate_email():

    alphabet = string.ascii_letters + string.digits

    while True:

        email_address = ''.join(secrets.choice(alphabet) for i in range(6))

        if (any(c.islower() for c in email_address)
              and any(c.isupper() for c in email_address)
              and sum(c.isdigit() for c in email_address) >= 3):
           break

    email_address = email_address + '@' + mastodon_hostname

    return email_address

def generate_pass():

    alphabet = string.ascii_letters + string.digits

    while True:

        password = ''.join(secrets.choice(alphabet) for i in range(10))

        if (any(c.islower() for c in password)
              and any(c.isupper() for c in password)
              and sum(c.isdigit() for c in password) >= 3):
           break

    return password

def register_user(username, passwd):

    user_email = generate_email()

    data = {'email':user_email,
            'full_name':username,
            'login_name':username,
            'must_change_password':True,
            'password':passwd,
            'restricted':False,
            'send_notify':True,
            'source_id':'0',
            'username':username,
            'visibility':'public'
        }
    API_ENDPOINT = 'https://' + gitea_hostname + '/api/v1/admin/users?'
    response = requests.post(url = API_ENDPOINT + 'access_token=' + gitea_access_token, data = data)

    is_registered = response.ok

    return is_registered

def get_data(notif):

    notification_id = notif.id

    if notif.type != 'mention':

        print(f'dismissing notification {notification_id}')

        mastodon.notifications_dismiss(notification_id)

        return

    account_id = notif.account.id

    username = notif.account.acct
    
    if '@' in username:

        print(f'dismissing notification {notification_id}')

        mastodon.notifications_dismiss(notification_id)

        return

    status_id = notif.status.id

    text  = notif.status.content

    visibility = notif.status.visibility

    reply, query_word = replying(text)

    if reply:

        if query_word == 'registre':

            user_pass = generate_pass()

            is_registered = register_user(username, user_pass)

            if is_registered:

                toot_text = f'@{username} registrat amb èxit!\n\n'

                toot_text += f'instància Gitea: https://{gitea_hostname} \n'

                toot_text += f'usuari: {username}\n'

                toot_text += f'contrasenya: {user_pass}\n\n'

                toot_text += "nota: aquesta contrasenya s'ha generat aleatòriament però l'hauràs de canviar "

                toot_text += f"desprès d'iniciar sessió a {gitea_hostname}\n\n"

                toot_text += f"Compte! a {gitea_hostname} tens assignada una adreça de correu aleatoria però falsa, sota el domini mastodont.cat. "

                toot_text += "L'hauràs de canviar per a poder rebre notificacions per correu electrònic."

                mastodon.status_post(toot_text, in_reply_to_id=status_id,visibility='direct')

                mastodon.notifications_dismiss(notification_id)

            else:

                toot_text = f'@{username} error en el registre!'

                mastodon.status_post(toot_text, in_reply_to_id=status_id,visibility=visibility)

                mastodon.notifications_dismiss(notification_id)

        else:

            mastodon.status_post(toot_text, in_reply_to_id=status_id,visibility=visibility)

            mastodon.notifications_dismiss(notification_id)

def replying(text):

    reply = False

    content = cleanhtml(text)
    content = unescape(content)

    try:

        start = content.index("@")

        end = content.index(" ")

        if len(content) > end:

            content = content[0: start:] + content[end +1::]

        cleanit = content.count('@')

        i = 0
        while i < cleanit :

            start = content.rfind("@")
            end = len(content)
            content = content[0: start:] + content[end +1::]
            i += 1

        content = content.lower()
        
        query_word = content

        if query_word == 'registre':

            reply = True

    except:

        pass

    return (reply, query_word)

def log_in():

    # Load secrets from secrets file
    secrets_filepath = "secrets/secrets.txt"
    uc_client_id     = get_parameter("uc_client_id",     secrets_filepath)
    uc_client_secret = get_parameter("uc_client_secret", secrets_filepath)
    uc_access_token  = get_parameter("uc_access_token",  secrets_filepath)

    # Load configuration from config file
    config_filepath = "config/config.txt"
    mastodon_hostname = get_parameter("mastodon_hostname", config_filepath)

    # Initialise Mastodon API
    mastodon = Mastodon(
        client_id = uc_client_id,
        client_secret = uc_client_secret,
        access_token = uc_access_token,
        api_base_url = 'https://' + mastodon_hostname,
    )

    # Initialise access headers
    headers={ 'Authorization': 'Bearer %s'%uc_access_token }

    return (mastodon, mastodon_hostname)

def get_gitea_hostname():

    config_filepath = "config/config.txt"
    gitea_hostname = get_parameter("gitea_hostname", config_filepath)
    gitea_access_token = get_parameter("gitea_access_token", config_filepath)

    return (gitea_hostname, gitea_access_token)

def get_parameter( parameter, file_path ):

    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, exiting."%file_path)
        sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

###############################################################################
# main

if __name__ == '__main__':

    mastodon, mastodon_hostname = log_in()

    gitea_hostname, gitea_access_token = get_gitea_hostname()

    notifications = mastodon.notifications()

    for notif in notifications:

        get_data(notif)
